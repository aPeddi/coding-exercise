//
//  _0210705_AvinashPeddi_NYCSchoolsApp.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 7/5/21.
//

import SwiftUI

@main
struct _0210705_AvinashPeddi_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
			SchoolFinderView()
        }
    }
}
