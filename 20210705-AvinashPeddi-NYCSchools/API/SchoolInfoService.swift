//
//  APIService.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 07/05/2021.
//  Copyright © 2020 Avinash Peddi. All rights reserved.
//

import Foundation
import Combine


enum SchoolInfoService {
	///Base URL for fetch Schools List
	static var baseURL: String {
		get {
			return "https://data.cityofnewyork.us/resource/s3k6-pzi2"
		}
	}
	
	///Detail URL for fetching specific school's information
	static var detailURL: String {
		get {
			return "https://data.cityofnewyork.us/resource/f9bf-2cp4?dbn="
		}
	}
}
