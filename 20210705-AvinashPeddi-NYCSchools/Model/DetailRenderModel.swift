//
//  DetailRenderModel.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 7/5/21.
//

import Foundation
import SwiftUI

struct DetailRenderModel: Identifiable, Equatable {
	let id: String = UUID().uuidString
	let schoolName: String
	let testTakerCount: String
	let readingScore: String
	let mathScore: String
	let writingScore: String
	
	enum CodingKeys: String, CodingKey {
		case schoolName = "school_name"
		case testTakerCount = "num_of_sat_test_takers"
		case readingScore = "sat_critical_reading_avg_score"
		case mathScore = "sat_math_avg_score"
		case writingScore = "sat_writing_avg_score"
	}
}

extension DetailRenderModel {
	//akin to convenience init
	public init() {
		self.init(schoolName: "",
				  testTakerCount: "",
				  readingScore: "",
				  mathScore: "",
				  writingScore: "")
	}
}

