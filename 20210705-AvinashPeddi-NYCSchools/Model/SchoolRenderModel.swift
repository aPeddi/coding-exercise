//
//  SchoolRenderModel.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 07/05/2021.
//  Copyright © 2020 Avinash Peddi. All rights reserved.
//

import Foundation
import SwiftUI

struct SchoolRenderModel: Identifiable {
	let id: String = UUID().uuidString
	let schoolName: String
	
	
	enum CodingKeys: String, CodingKey {
		case schoolName = "school_name"
	}
}
