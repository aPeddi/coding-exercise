//
//  SATDetails.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 7/5/21.
//

import Foundation


struct SchoolSATDetails: Codable {
	let dbn: String
	let schoolName: String
	let testTakerCount: String
	let readingScore: String
	let mathScore: String
	let writingScore: String
	
	enum CodingKeys: String, CodingKey {
		case dbn
		case schoolName = "school_name"
		case testTakerCount = "num_of_sat_test_takers"
		case readingScore = "sat_critical_reading_avg_score"
		case mathScore = "sat_math_avg_score"
		case writingScore = "sat_writing_avg_score"
	}
}
