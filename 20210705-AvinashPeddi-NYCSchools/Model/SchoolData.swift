//
//  School.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 07/05/2021.
//  Copyright © 2020 Avinash Peddi. All rights reserved.
//

import Foundation

struct SchoolData: Codable {
	let dbn: String
	let schoolName: String
	
	enum CodingKeys: String, CodingKey {
		case dbn
		case schoolName = "school_name"
	}
}
