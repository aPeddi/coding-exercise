//
//  SchoolDetail.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 07/05/2021.
//  Copyright © 2020 Avinash Peddi. All rights reserved.
//

import SwiftUI

///Detail Page Implementation for School
struct SchoolDetail : View {
	let schoolId : String
	@ObservedObject var viewModel: SchoolFinderViewModel
	@State private var showingAlert = false
	
	init(schoolId: String, viewModel: SchoolFinderViewModel) {
		self.schoolId = schoolId
		self.viewModel = viewModel
	}
	
	var body: some View {
		ScrollView {
			VStack {
				Text(viewModel.detail.schoolName)
					.font(.title)
					.multilineTextAlignment(.center)
					.lineLimit(nil)
					.padding()
				
				Text("SAT Scores")
					.font(.title).bold()
					.multilineTextAlignment(.center)
					.lineLimit(nil)
					.padding()
				
				Text("Reading: "+viewModel.detail.readingScore)
					.font(.headline)
					.multilineTextAlignment(.center)
					.lineLimit(nil)
					.padding()
				
				Text("Math: "+viewModel.detail.mathScore)
					.font(.headline)
					.multilineTextAlignment(.center)
					.lineLimit(nil)
					.padding()
				
				Text("Writing: "+viewModel.detail.writingScore)
					.font(.headline)
					.multilineTextAlignment(.center)
					.lineLimit(nil)
					.padding()
			}.alert(isPresented: $showingAlert) {
				Alert(title: Text("Error"), message: Text("No Data Found"), dismissButton: .default(Text("OK")))
			}.onChange(of: viewModel.showAlert, perform: { value in
				showingAlert = value
			})
		}
		.onAppear(perform: {
			viewModel.loadDetails(forId: schoolId)
		})
	}
}
