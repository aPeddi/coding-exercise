//
//  SchoolFinderView.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 07/05/2021.
//  Copyright © 2020 Avinash Peddi. All rights reserved.
//

import SwiftUI

struct SchoolFinderView : View {
	@ObservedObject private var viewModel = SchoolFinderViewModel()
	
	var body: some View {
		NavigationView {
			VStack {
				List(viewModel.items) { item in
					NavigationLink(destination: SchoolDetail(schoolId: item.id, viewModel: viewModel)) {
						SchoolCell(renderModel: item)
					}
				}
			}
			.navigationBarTitle("NYC Schools", displayMode: .automatic)
		}
	}
}

#if DEBUG
struct SchoolFinderView_Previews : PreviewProvider {
	static var previews: some View {
		SchoolFinderView()
	}
}
#endif
