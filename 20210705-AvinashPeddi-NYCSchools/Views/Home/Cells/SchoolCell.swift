//
//  SchoolCell.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 07/05/2021.
//  Copyright © 2020 Avinash Peddi. All rights reserved.
//

import SwiftUI

struct SchoolCell: View {
	private let renderModel: SchoolRenderModel
	
	init(renderModel: SchoolRenderModel) {
		self.renderModel = renderModel
	}
	
	var body: some View {
		HStack {
			Text(renderModel.schoolName).font(.headline)
		}
		.frame(height: 88)
	}
}

#if DEBUG
let schoolDemoData = SchoolRenderModel.init(schoolName: "")

struct SchoolCell_Previews : PreviewProvider {
	static var previews: some View {
		SchoolCell(renderModel: schoolDemoData)
			.previewLayout(.fixed(width: 300, height: 65))
			.previewDisplayName("SchoolCell")
	}
}
#endif

