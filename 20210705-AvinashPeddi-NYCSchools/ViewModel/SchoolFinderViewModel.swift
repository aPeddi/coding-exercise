//
//  SchoolFinderViewModel.swift
//  20210705-AvinashPeddi-NYCSchools
//
//  Created by Avinash Peddi on 07/05/2021.
//  Copyright © 2020 Avinash Peddi. All rights reserved.
//

import SwiftUI
import Combine

final class SchoolFinderViewModel: ObservableObject {
	@Published private (set) var items = [SchoolRenderModel]()
	@Published private (set) var detail = DetailRenderModel()
	@Published private (set) var schools = [SchoolData]()
	@Published var showAlert: Bool = false{
		didSet{
			self.objectWillChange.send()
		}
	}
	
	typealias DataHandler = (Data?, URLResponse?, Error?) -> Void
	
	init () {
		loadSchools(){data, response, error in
			if error == nil {
				do {
					guard let data = data else { return }
					let feed = try JSONDecoder().decode([SchoolData].self, from: data)
					print(feed)
					DispatchQueue.main.async {
						self.schools = feed
						
						/// converts data model to a generic render model to be passed on to the view
						self.items = feed.map({ item in
							SchoolRenderModel.init(schoolName: item.schoolName)
						})
					}
				} catch {
					print(error, error.localizedDescription, "JSON processing failed")
				}
			} else {
				print(error?.localizedDescription ?? "Unknown Error Retrieving Albums")
			}
		}
	}
	
	/// loads a list of high schools in NYC
	func loadSchools(completion: @escaping DataHandler) {
		guard let feedUrl = URL.init(string: SchoolInfoService.baseURL) else { return }
		URLSession.shared.dataTask(with: feedUrl) { (data, response, error) in
			completion(data,response,error)
		}.resume()
	}
	
	/// loads details of School's SAT Scores
	/// - Parameter id: id of the school item in the list
	func loadDetails(forId id: String) {
		self.showAlert = false
		detail = DetailRenderModel()
		if let currentIndex = items.firstIndex(where: { $0.id == id }) {
			guard let feedUrl = URL.init(string: SchoolInfoService.detailURL + schools[currentIndex].dbn) else { return }
			URLSession.shared.dataTask(with: feedUrl) { (data, response, error) in
				if error == nil {
					do {
						guard let data = data else { return }
						if let item = try JSONDecoder().decode([SchoolSATDetails].self, from: data).first {
							DispatchQueue.main.async {
								/// converts data model to a generic render model to be passed on to the view
								self.detail = DetailRenderModel.init(schoolName: item.schoolName, testTakerCount: item.testTakerCount, readingScore: item.readingScore, mathScore: item.mathScore, writingScore: item.writingScore)
							}
						} else {
							DispatchQueue.main.async {
								self.showAlert = true
							}
						}
					} catch {
						print(error, error.localizedDescription, "JSON processing failed")
					}
				} else {
					print(error?.localizedDescription ?? "Unknown Error Retrieving Albums")
				}
			}.resume()
		}
	}
}
