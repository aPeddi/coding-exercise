//
//  _0210705_AvinashPeddi_NYCSchoolsTests.swift
//  20210705-AvinashPeddi-NYCSchoolsTests
//
//  Created by Avinash Peddi on 7/5/21.
//

import XCTest
@testable import _0210705_AvinashPeddi_NYCSchools

class _0210705_AvinashPeddi_NYCSchoolsTests: XCTestCase {

	func testSuccessScenarioWhenLoadingSchools() {
		let viewModel = SchoolFinderViewModel.init()
		let expect = expectation(description: "Test Loading Schools")
				
		viewModel.loadSchools() { (data, responce, error) in
			DispatchQueue.main.async {
				if (error != nil) || (data == nil) {
					XCTFail("error: \(String(describing: error))")
				}
				expect.fulfill()
			}
		}
		//then
		waitForExpectations(timeout: 2) { (error) in
			if let error = error {
				XCTFail("Error: \(error)")
			}
		}
	}

}
